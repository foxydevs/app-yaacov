import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { HomeGuard } from './_guard/home.guard';
import { AuthGuard } from './_guard/auth.guard';

const routes: Routes = [
  {
    path: 'cliente',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule), canActivate: [AuthGuard]
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule', canActivate: [HomeGuard] },
  { path: 'tab', loadChildren:  './tab1/tab1.module#Tab1PageModule', canActivate: [AuthGuard] },
  { path: 'subcategoria/:id', loadChildren: './tabs/subcategoria/subcategoria.module#SubcategoriaPageModule', canActivate: [AuthGuard] },
  { path: 'producto/:id', loadChildren: './tabs/producto/producto.module#ProductoPageModule', canActivate: [AuthGuard] },
  { path: 'carrito', loadChildren: './tabs/carrito/carrito.module#CarritoPageModule', canActivate: [AuthGuard] },
  { path: 'direccion', loadChildren: './tabs/direccion/direccion.module#DireccionPageModule', canActivate: [AuthGuard] },
  { path: 'compra', loadChildren: './tabs/compra/compra.module#CompraPageModule', canActivate: [AuthGuard] },
  { path: 'mi-compra/:type', loadChildren: './tabs/mi-compra/mi-compra.module#MiCompraPageModule', canActivate: [AuthGuard] },
  {
    path: '',
    redirectTo: 'cliente',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
