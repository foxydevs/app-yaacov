import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'cliente',
    component: TabsPage,
    children: [
      {
        path: 'categoria',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./categoria/categoria.module').then(m => m.CategoriaPageModule)
          }
        ]
      },
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./dashboard/dashboard.module').then(m => m.DashboardPageModule)
          }
        ]
      },
      {
        path: 'promocion',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./promocion/promocion.module').then(m => m.PromocionPageModule)
          }
        ]
      },
      {
        path: 'informacion',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./informacion/informacion.module').then(m => m.InformacionPageModule)
          }
        ]
      },
      {
        path: 'perfil',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./perfil/perfil.module').then(m => m.PerfilPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/cliente/cliente/categoria',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/cliente/cliente/categoria',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
