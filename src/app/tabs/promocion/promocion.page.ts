import { Component, OnInit } from '@angular/core';
import { PromocionService } from 'src/app/_service/promocion.service';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-promocion',
  templateUrl: './promocion.page.html',
  styleUrls: ['./promocion.page.scss'],
})
export class PromocionPage implements OnInit {
  public promociones:any[] = [];
  public descuentos:any[] = [];
  selectItem:any = 'promocion';
  constructor(
    private mainService: PromocionService,
    private clipboard: Clipboard,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getAllPromocion();
  }

  segmentChanged(ev: any) {
    this.selectItem = ev.detail.value;
    //console.log('Segment changed', ev);
    if(ev.detail.value == 'promocion') {
      this.getAllPromocion();
    } else if(ev.detail.value == 'descuento') {
      this.getAllDescuento();
    }
  }

  public getAllPromocion() {
    this.mainService.getByUser(24)
    .subscribe(res => {
      //console.log(res)
      this.promociones = []
      this.promociones = res;
    }, (error) => {
      //console.log(error)
    })
  }

  public getAllDescuento() {
    this.mainService.getByDescuentos(24)
    .subscribe(res => {
      //console.log(res)
      this.descuentos = []
      this.descuentos = res;
    }, (error) => {
      //console.log(error)
    })
  }

  public copy(code:string) {
    this.clipboard.copy(code);
    this.notificationService.alertToast('Codigo copiado al portapapeles.');
  }

}
