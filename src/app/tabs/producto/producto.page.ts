import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { ProductoService } from 'src/app/_service/producto.service';
import { path } from 'src/app/config.module';
import { Location } from '@angular/common';
import { ModalProductoComponent } from './modal-producto/modal-producto.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.page.html',
  styleUrls: ['./producto.page.scss'],
})
export class ProductoPage implements OnInit {
  search:string;
  parameter:any;
  selectItem:any;
  productos:any[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificacionService,
    private productService: ProductoService,
    private router: Router,
    private location: Location,
    private modalController: ModalController
  ) {
    this.parameter = +this.activatedRoute.snapshot.paramMap.get('id');
    this.getAllByUser(path.id)
  }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  getAllByUser = (id:number) => {
    this.notificationService.alertLoading('Cargando...', 10000);
    this.productService.getByUser(id)
    .subscribe((res)=>{
      //console.log(res)
      this.productos = [];
      res.forEach(data => {
        if(data.category==this.parameter) {
          this.productos.push(data)
        }
      });
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();      
    });
  }

  //AÑADIR AL CARRITO
  saveChanges2 = (product:any) => {
    if(this.selectItem) {
      let data = {
        cantidad: 1,
        name: product.name,
        subtotal: this.selectItem.price,
        precioClienteEs: null,
        precioDistribuidor: null, 
        producto: product.id,
        picture: product.picture,
        price: this.selectItem.price,
        precioVenta: this.selectItem.price,
        presentacion: this.selectItem.id,
        opcion1: this.selectItem.id,
        opcion2: this.selectItem.description,
        opcion3: null,
        opcion4: null,
        opcion5: null,
        opcion6: null,
        state: 4,
        comment: null
      }
      let cart:any[] = []
      cart = JSON.parse(localStorage.getItem('cart'))
      cart.push(data)
      localStorage.removeItem('cart')
      localStorage.setItem('cart', JSON.stringify(cart));
      this.notificationService.alertToast(data.name + ' agregado al carrito :D');
      this.selectItem = "";
    }
  }

  //AÑADIR AL CARRITO
  saveChanges = (product:any) =>  {
    console.log(product)
    let data = {
      cantidad: 1,
      name: product.name,
      subtotal: product.price,
      price: product.price,
      precioVenta: product.price,
      precioClienteEs: null,
      precioDistribuidor: null, 
      presentacion: null,
      producto: product.id,
      picture: product.picture,
      opcion1: null,
      opcion2: null,
      opcion3: null,
      opcion4: null,
      opcion5: null,
      opcion6: null,
      state: 4,
      comment: null
    }
    console.log(data)
    let cart:any[] = []
    cart = JSON.parse(localStorage.getItem('cart'))
    cart.push(data)
    localStorage.removeItem('cart')
    localStorage.setItem('cart', JSON.stringify(cart));
    this.notificationService.alertToast(data.name + ' agregado al carrito :D');
  }

  async presentModal(id:any) {
    const modal = await this.modalController.create({
      component: ModalProductoComponent,
      componentProps: {
        value: id
      }
    });

    modal.onDidDismiss().then((data: any) => {
      
    });

    return await modal.present();
  }

}
