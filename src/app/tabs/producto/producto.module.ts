import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ProductoPage } from './producto.page';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ModalProductoComponent } from './modal-producto/modal-producto.component';

const routes: Routes = [
  {
    path: '',
    component: ProductoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Ng2SearchPipeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ProductoPage,
    ModalProductoComponent
  ], entryComponents: [
    ModalProductoComponent
  ]
})
export class ProductoPageModule {}
