import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MiCompraPage } from './mi-compra.page';
import { ModalMiCompraComponent } from './modal-mi-compra/modal-mi-compra.component';

const routes: Routes = [
  {
    path: '',
    component: MiCompraPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    MiCompraPage,
    ModalMiCompraComponent
  ], entryComponents: [
    ModalMiCompraComponent
  ]
})
export class MiCompraPageModule {}
