import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { OrdenService } from 'src/app/_service/orden.service';
import { VentaService } from 'src/app/_service/venta.service';

@Component({
  selector: 'app-modal-mi-compra',
  templateUrl: './modal-mi-compra.component.html',
  styleUrls: ['./modal-mi-compra.component.scss'],
})
export class ModalMiCompraComponent implements OnInit {
  title:string;
  parameter:any;
  data:any;
  data2:any;
  total:number = 0;

  constructor(
    private modalController: ModalController,
    private ordenService: OrdenService,
    private ventaService: VentaService,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    this.getSingle(this.parameter);
  }

  closeModal() {
    this.modalController.dismiss();
  }

  getSingle(id:number) {
    this.ordenService.getSingle(id)
    .subscribe((res) => {
      console.log(res)
      this.data = res;
      this.getVenta(res.venta)
    }, (error) => {
      console.log(error)
    })
  }

  getVenta(id:number) {
    this.ventaService.getSingle(id)
    .subscribe((res) => {
      console.log(res)
      this.data2 = res;
      res.detalle.forEach(element => {
        console.log(element)
        this.total = this.total + element.subtotal;
      });
    }, (error) => {
      console.log(error)
    })
  }
}
