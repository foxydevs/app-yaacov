import { Component, OnInit } from '@angular/core';
import { path } from 'src/app/config.module';
import { DireccionService } from 'src/app/_service/direccion.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';
import { AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { DescuentoService } from 'src/app/_service/descuento.service';
import { OrdenService } from 'src/app/_service/orden.service';
import { VentaService } from 'src/app/_service/venta.service';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

//JQUERY
declare var $:any;

@Component({
  selector: 'app-compra',
  templateUrl: './compra.page.html',
  styleUrls: ['./compra.page.scss'],
})
export class CompraPage implements OnInit {
  public data = {
    id: 24,
    cliente: +localStorage.getItem('currentId'),
    infodeposito: localStorage.getItem('currentDeposito'),
    usuario: 24,
    total: 0,
    nombre: '',
    unit_price: 0,
    cantidad: 0,
    tipo: 0,
    tipoPlazo: '',
    plazo: '',
    comment: '',
    deposito: '',
    direccion: '',
    fecha: '',
    image: '',
    state: 4,
    comprobante: 1,
    detalle: [],
    archivo: 'https://bpresentacion.s3-us-west-2.amazonaws.com/Desarrollo/yaacov.png',
    x_product_id: '1', //1
    x_currency_code: 'GTQ', //1
    x_amount: 0.0, //1
    x_line_item: '1',  //
    x_freight: '0', //
    x_email: '', //1
    cc_number: '', //1
    cc_exp: '', //1
    cc_cvv2: '', //1
    cc_name: '', //1
    cc_type: '', //1
    x_first_name: '', //1
    x_last_name: '', //1
    x_company: '1',  //1
    x_address: '', //1
    x_city: '1', //1
    x_state: '1', //1
    x_country: '1', //1
    x_zip: '1', //1
    http_origin: 'none', //??
    visaencuotas: '0', //NO VISACUOTAS
  }
  public data2 = {
    codigo: '',
    latitude: 0,
    cantidad: 0,
    id: 0
  }
  public url:any;
  public cart:any[] = [];
  public Ventas:any[] = [];
  public table:any[] = [];
  disabledBtn:boolean;
  btnDisabled2:boolean;
  public basePath:string = path.path;
  public userID:string = path.id;
  navColor = localStorage.getItem('currentColor');
  btnColor = localStorage.getItem('currentColorButton');

  constructor(
  public mainService: OrdenService,
  public secondService: DireccionService,
  public thirdService: VentaService,
  public fourthService: DescuentoService,
  private notificationService: NotificacionService,
  private alertController: AlertController,
  private location: Location,
  private router: Router
  ) {
    //this.loadInvoice();
    this.cart = JSON.parse(localStorage.getItem('cart'));
    this.data.x_first_name = localStorage.getItem('currentFirstName');
    this.data.x_last_name = localStorage.getItem('currentLastName');
    this.data.x_email = localStorage.getItem('currentEmail');
    this.data.detalle = this.cart;
    
  }

  goToBack() {
    this.location.back();
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  ionViewWillEnter() {
    this.getAllSecond();
  }
 
  ngOnInit() {
    $('#imgAvatar2').hide();
    this.getDate();
    this.dataCart();
    this.getAll();
  }

  //OBTENER FECHA
  public getDate() {
    let date = new Date();
    let month = date.getMonth()+1;
    let month2;
    let dia= date.getDate();
    let dia2;
    if(month<10){
      month2='0'+month;
    }else{
      month2=month
    }
    if(dia<10){
      dia2='0'+dia;
    }else{
      dia2=dia
    }
    this.data.fecha = date.getFullYear()+'-'+month2+'-'+dia2;
  }

  async saveChanges() {
    this.saveChanges2();

    /*const alert = await this.alertController.create({
      header: 'Información de Entregas',
      message: 'Todos los pedidos recibidos antes de las 6PM serán entregados al siguiente día hábil que la ruta pase en su zona. Carretera al Salvador, San José Pinula, Z. 15, Z. 10, Z. 13, Z. 9 y Z. 16: todos los días. Mixco, San Cristobal y el Naranjo: Jueves. Para poderte llevar a domicilio el mínimo de pedido es de Q50.00.',
      buttons: [
        {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
            this.saveChanges2();
          }
        }
      ]
    });

    await alert.present();*/
  }

  validarDatos() {
    if(this.data.tipo) {
      if(this.data.x_address) {
        this.saveChanges();
      } else {
        this.notificationService.alertToast('La dirección es requerida.');
      }
    } else {
      this.notificationService.alertToast('El Tipo de Venta es requerido.')
    }
    
  }

  public saveChanges2() {
    this.data.cc_exp = moment(this.data.cc_exp).format('MM/YY');
    console.log(this.data.cc_exp)
    this.data.archivo = $('img[alt="Avatar"]').attr('src');
    if(this.data.tipo == 3) {        
        this.data.x_line_item = 'Varios<|>00000<|><|>' + this.data.cantidad + '<|>' + this.data.x_amount + '<|>';
        this.mainService.pagarQPP(this.data)
        .subscribe((res) => {
          console.log(res);
          console.log(this.data);
          this.messagesQPayPro(res.responseCode, res.responseText);
          this.updateDescuento();
        }, (error) => {
          console.log(error)
          this.notificationService.alertMessage('QPayPro Error', error);
          console.clear
        });
    } else {
        this.data.direccion = this.data.x_address;
       if(this.data.tipo == 2) {
          this.data.state = 5;
        }
        if(this.data.archivo == 'https://bpresentacion.s3.us-west-2.amazonaws.com/products/M7Xjg9Ih4fCEhK5SQ2y2r3Go70pv9YJ2jgx7CcQt.png'
        || this.data.archivo == this.data.image) {
          if(this.data.direccion) {
            this.data.archivo = '';
            this.create(this.data);
          } else {
            this.notificationService.alertToast('La dirección es requerida.');
          }
        } else {
          if(this.data.direccion) {
            console.log(this.data)
            this.create(this.data);
          } else {
            this.notificationService.alertToast('La dirección es requerida.');
          }
        }
    }
  }

  //AGREGAR
  create(formValue:any) {
    this.disabledBtn = true;
    this.thirdService.create(formValue)
    .subscribe((res) => {
      this.notificationService.alertMessage('Venta Agregada', 'La venta fue agregada exitosamente.');
      localStorage.setItem('cart', JSON.stringify([]));      
      //this.navCtrl.setRoot('MyOrdersClientPage');
      this.goToRoute('mi-compra/compra');
      this.updateDescuento();
    }, (error) => {
      this.disabledBtn = false;
      console.clear
    });
  }

  //CARGAR PRODUCTOS
  public getAll() {
    this.thirdService.getAllTypes()
    .subscribe((res) => {
      console.log(res)
      this.Ventas = [];
      this.Ventas = res;
    }, (error) => {
      console.clear
    })
  }

  //IMPRESIÓN DE ERRORES
  public messagesQPayPro(responseCode:any, responseText:string) {
    if(responseCode == '00') {
      this.data.state = 3;
      this.disabledBtn = true;
      this.thirdService.create(this.data)
      .subscribe((res) => {
        this.notificationService.alertMessage('QPayPro - ' + responseCode, responseText);
        localStorage.setItem('cart', JSON.stringify([]));      
        //this.navCtrl.setRoot('MyOrdersClientPage');
        //IR A MIS COMPRAS
        this.goToRoute('mi-compra/compra');
      }, (error) => {
        this.disabledBtn = false;
        console.clear
      });
    } else {
      this.notificationService.alertMessage('QPayPro - ' + responseCode, responseText);
      this.disabledBtn = true;
    }
  }

  //DETALLES
  public dataCart() {
    let a:number = 0;
    let b:number = 0;
    let c:number = 0;
    let d:number = 0;
    for(let x of this.cart) {
      a = x.subtotal;
      b += a;
      c = +x.cantidad;
      d += c;
    }
    this.data.x_amount = b;
    this.data.total = b;
    this.data.unit_price = b;
    this.data.cantidad = d;
  }

  public generate(longitude)
  {
    let i:number
    var caracteres = "123456789+-*abcdefghijkmnpqrtuvwxyz123456789+-*ABCDEFGHIJKLMNPQRTUVWXYZ12346789+-*";
    var password = "";
    for (i=0; i<longitude; i++) password += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
    return '02' + password;
  }

  //IMAGEN DE ORDEN
  uploadImage(archivo, id) {
    var archivos = archivo.srcElement.files;
    let url = `${this.basePath}pictures/upload`;

    var size=archivos[0].size;
    var type=archivos[0].type;
    if(type == "image/png" || type == "image/jpeg" || type == "image/jpg" || type == 'application/pdf') {
      if(size<(2*(1024*1024))) {
        $('#imgAvatar').attr("src",'https://www.oriconsultas.com/afiliacion/Consultas/master_css/css_menu/icon/gif_carga.gif');
        $("#"+id).upload(url,
          {
            avatar: archivos[0],
            carpeta: 'orders'
          },
          function(respuesta) {
            if(type == 'application/pdf') {
              $('#imgAvatar').attr("src", respuesta.url);
              $('#imgAvatar2').show();
              $('#imgAvatar').hide();              
              $('#imgAvatar2').attr("src", 'https://developers.zamzar.com/assets/app/img/convert/pdf.png');
              $("#"+id).val('');
            } else {
              $('#imgAvatar').attr("src", respuesta.url);
              $("#"+id).val('');
            }
          }
        );
      } else {
        this.notificationService.alertToast('El archivo es demasiado grande.')
      }
    } else {
      this.notificationService.alertToast('El tipo de archivo no es válido.')
    }
  }

  //CARGAR LOS RETOS
  public getAllSecond() {
    this.notificationService.alertLoading('Cargando...', 10000);
    this.secondService.getAll()
    .subscribe(res => {
      this.table = [];
      this.table = res;
      this.notificationService.dismiss();
    }, (error) => {
      this.notificationService.dismiss();
      console.clear
    })
  }

  //GET SINGLE
  search() {
    if(this.data2.codigo) {
      this.notificationService.alertLoading('Cargando...', 10000);
      this.fourthService.search(path.id, this.data2.codigo, 'codigo')
      .subscribe((res) => {
          console.log(res)
          if(res) {
            if(res[0].cantidad > 0) {
              this.btnDisabled2 = true;
              this.data2 = res[0];
              this.data.unit_price =  this.data.unit_price - (+this.data.unit_price * (this.data2.latitude/100));;
              this.data.x_amount =  this.data.x_amount - (+this.data.x_amount * (this.data2.latitude/100));;
              this.data.total =  this.data.x_amount;
              this.notificationService.alertMessage('Código Encontrado', 'El código de descuento ha sido aplicado exitosamente su descuento es de '+ res[0].latitude.toString() + '%');
              this.data2.cantidad = this.data2.cantidad - 1;
            } else {
              this.notificationService.alertMessage('Código No Disponible', 'Ya no queda disponibilidad para canjear codigo.');
            }
            this.notificationService.dismiss();
        }        
      }, (error) => {
        this.notificationService.dismiss();
      })
    } else {
      this.notificationService.alertToast('El codigo es requerido.');
    }
  }

  //UPDATE
  public updateDescuento() {
    this.fourthService.update(this.data2)
    .subscribe((res) => {
      console.log(res)
    }, (error) => {
      console.log(error)
    })
  }

  onChange(e:any) {
    this.dataCart()
    if(e == 1) {
      this.data.total = this.data.total + 25;
    } else {
      this.data.total = this.data.total + 35;
    }
  }
 
}
