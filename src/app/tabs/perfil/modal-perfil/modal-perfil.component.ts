import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-perfil',
  templateUrl: './modal-perfil.component.html',
  styleUrls: ['./modal-perfil.component.scss'],
})
export class ModalPerfilComponent implements OnInit {
  parameter:string;
  title:string;
  changePassword = {
    old_pass: '',
	  new_pass : '',
	  new_pass_rep: '',
    id: +localStorage.getItem('currentId')
  }
  profile = {
    picture: '',
    username: '',
    email: '',
    firstname: '',
    lastname: '',
    work: '',
    description: '',
    age: '',
    birthday: '',
    phone: '',
    referencia: null,
    inicioMembresia: '',
    finMembresia: '',
    tipoNivel: '',
    opcion18: '',
    referencias: [],
    id: localStorage.getItem('currentId')
  }
  btnDisabled:boolean;

  constructor(
    private navParams: NavParams,
    private modalController: ModalController,
    private mainService: UsuarioService,
    private notificationService: NotificacionService
  ) { }

  ngOnInit() {
    this.parameter = this.navParams.get('value');
    if(this.parameter == '1') {
      this.title = 'Actualizar Perfil';
      this.getSingle(this.profile.id);
    } else if(this.parameter == '2') {
      this.title = 'Cambiar Contraseña';
    }
  }

  //CARGAR USUARIO
  public getSingle(id:any) {
    this.mainService.getSingle(id)
    .subscribe((res) => {
      this.profile = res;
      console.log(res)
    }, (error) => {
      console.log(error)
    })
  }

  //CAMBIAR CONTRASEÑA
  changePasswordProfile() {
    if(this.changePassword.old_pass == this.changePassword.new_pass) {
      this.notificationService.alertToast('No se puede usar la misma contraseña.');
    } else {
      if(this.changePassword.new_pass.length >= 8) {
        if(this.changePassword.new_pass == this.changePassword.new_pass_rep) {
          this.btnDisabled = true;
          this.mainService.changePassword(this.changePassword)
          .subscribe((res) => {
            this.closeModal();
            this.notificationService.alertMessage('Contraseña Actualizada', 'La contraseña ha sido actualizada exitosamente.');
          }, (error) => {
            this.btnDisabled = false;
            this.notificationService.alertToast('Contraseña inválida.')
          })
        } else {
          this.notificationService.alertToast('Las contraseñas no coinciden.');
        }
      } else {
        this.notificationService.alertToast('La contraseña debe contener al menos 8 caracteres.');        
      }
    }
  }

  public updateProfile() {
    let cumple = this.profile.birthday.split("T");
    this.profile.birthday = cumple[0];
    this.btnDisabled = true;
    this.mainService.update(this.profile)
    .subscribe(res => {
      localStorage.setItem('currentFirstName', this.profile.firstname);
      localStorage.setItem('currentLastName', this.profile.lastname);
      localStorage.setItem('currentEmail', this.profile.email);
      this.modalController.dismiss(res);
      this.notificationService.alertMessage('Perfil Actualizado', 'Tu perfil ha sido actualizado exitosamente.');
    }, (error) => {
      this.btnDisabled = false;
    });
  }

  closeModal() {
    this.modalController.dismiss();
  }

}
