import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { InformacionModalComponent } from './informacion-modal/informacion-modal.component';
import { InformacionService } from 'src/app/_service/informacion.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { UsuarioService } from 'src/app/_service/usuario.service';

@Component({
  selector: 'app-informacion',
  templateUrl: './informacion.page.html',
  styleUrls: ['./informacion.page.scss'],
})
export class InformacionPage implements OnInit {
  information:any[];
  data:any;
  parameter:any;

  constructor(
    private modalController: ModalController,
    public mainService: InformacionService,
    public secondService: UsuarioService,
    private iab: InAppBrowser,
    private callNumber: CallNumber
  ) {
    
  }

  ngOnInit() {
    this.getSingle();
    this.getAll();
  }

  async presentModal(id:number) {
    const modal = await this.modalController.create({
      component: InformacionModalComponent,
      componentProps: {
        value: id
      },
      cssClass: 'shareModal'
    });

    modal.onDidDismiss().then((data: any) => {
    });

    return await modal.present();
  }

  //CARGAR USUARIOS
  public getAll(){
    this.mainService.getByUser(24)
    .subscribe((res) => {
      //console.log(res)
      this.information = [];
      this.information = res;
    }, (error) => {
      //console.clear;
    })
  }

  public getSingle(){
    this.secondService.getSingle(24)
    .subscribe((res) => {
      //console.log(res)
      this.data = res;
    }, (error) => {
      //console.clear;
    })
  }

  openBrowser(url:any) {
    this.iab.create(url, '_system');
  }

  public callPhone(phone:string) {
    this.callNumber.callNumber(phone, true)
    .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }

}
