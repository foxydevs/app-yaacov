import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { ModalController } from '@ionic/angular';
import { AuthService } from '../_service/auth.service';
import { NotificacionService } from '../_service/notificacion.service';
import { ModalRegisterComponent } from './modal-register/modal-register.component';
import { ModalResetComponent } from './modal-reset/modal-reset.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private disabledBtn:boolean = false;
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private data = {
    username: '',
    password: ''
  }

  constructor(private router: Router,
    private location: Location,
    private mainService: AuthService,
    private notificacionService: NotificacionService,
    private modalController: ModalController) { }

  ngOnInit() {
  }

  goToRoute(route:string) {
    this.router.navigate([`${route}`])
  }

  goToBack() {
    this.location.back();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  authentication() {
    if(this.data.username) {
      if(this.data.password) {
        this.auth();
      } else {
        this.notificacionService.alertToast("La contraseña es requerida.");
      }
    } else {
      this.notificacionService.alertToast("El usuario es requerido.");
    }
  }

  auth() {
    //let events = this.events;
    this.disabledBtn = true;
    this.mainService.auth(this.data)
    .subscribe((res) => {
      console.log(res)
      localStorage.setItem('cart', JSON.stringify([]));
      localStorage.setItem('currentUser', res.username);
      localStorage.setItem('currentEmail', res.email);
      localStorage.setItem('currentId', res.id);             
      localStorage.setItem('currentPicture', res.picture);
      localStorage.setItem('currentFirstName', res.firstname);
      localStorage.setItem('currentLastName', res.lastname);
      localStorage.setItem('currentState', res.state);
      this.goToRoute('tab');
      this.disabledBtn = false;
    }, (error) => {
      console.error(error);
      this.disabledBtn = false;
      if(error.status == 401){
        this.notificacionService.alertToast('Usuario o contraseña incorrectos.');
      }else
      if(error.status == 400){
        this.notificacionService.alertToast('Su usuario no esta registrado en esta aplicacion.');
      }
    });
  }

  async presentModalRegistro() {
    const modal = await this.modalController.create({
      component: ModalRegisterComponent
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }

  async presentModalReset() {
    const modal = await this.modalController.create({
      component: ModalResetComponent
    });
    modal.onDidDismiss().then((data) => {
      //DATOS
    });
    return await modal.present();
  }

}
