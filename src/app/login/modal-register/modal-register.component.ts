import { Component, OnInit } from '@angular/core';
import { path } from 'src/app/config.module';
import { ModalController } from '@ionic/angular';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { NotificacionService } from 'src/app/_service/notificacion.service';

@Component({
  selector: 'app-modal-register',
  templateUrl: './modal-register.component.html',
  styleUrls: ['./modal-register.component.scss'],
})
export class ModalRegisterComponent implements OnInit {
  public applicationId:number = path.id;
  public title:string = 'Registro';
  private passwordType:string = 'password';
  private passwordShow:boolean = false;
  private passwordType2:string = 'password';
  private passwordShow2:boolean = false;
  public btnDisabled = false;
  public data:any = {
    username: '',
    email: '',
    password: '',
    password_repeat: '',
    usuario: this.applicationId,
    firstname: '',
    lastname: '',
    work: '',
    description: '',
    referencia: null,
    refer: 0,
    age: '',
    birthday: '2019-01-01',
    search: '',
    opcion18: '',
    empresa: '',
    phone: ''
  }
  constructor(
    private usuarioService: UsuarioService,
    private notificationService: NotificacionService,
    private modalController: ModalController
    ) { }

  ngOnInit() {}

  //CERRAR MODAL
  closeModal() {
    this.modalController.dismiss();
  }

  //TOGGLE PASSWORD
  togglePassword() {
    if(this.passwordShow) {
      this.passwordShow = false;
      this.passwordType = 'password';
    } else {
      this.passwordShow = true;
      this.passwordType = 'text';
    }
  }

  //TOGGLE PASSWORD
  togglePassword2() {
    if(this.passwordShow2) {
      this.passwordShow2 = false;
      this.passwordType2 = 'password';
    } else {
      this.passwordShow2 = true;
      this.passwordType2 = 'text';
    }
  }

  saveChanges() {
    if(this.data.username) {
      if(this.data.email) {
        if(this.data.password) {
          if(this.data.password_repeat) {
            if(this.data.password == this.data.password_repeat) {
              this.btnDisabled = true;
              this.create();
            } else {
              this.notificationService.alertToast('Las contraseñas deben ser iguales.')
            }
          } else {
            this.notificationService.alertToast('Confirmar contraseña es requerido.')
          }
        } else {
          this.notificationService.alertToast('La contraseña es requerida.')
        }
      } else {
        this.notificationService.alertToast('El correo electrónico es requerido.')
      }
    } else {
      this.notificationService.alertToast('El nombre de usuario es requerido.')
    }
  }

  public create() {
    this.usuarioService.create(this.data)
    .subscribe((res)=> {
      console.log(res)
      this.notificationService.alertMessage('Registro', 'Tu usuario se ha registrado exitosamente.');
      this.closeModal();
    }, (err) => {
      console.error(err);
      this.btnDisabled = false;
      if(err.status == '400') {
        this.notificationService.alertToast('El nombre de usuario o correo electrónico ya existe.');
      }
    });
  }

}
