import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { LoginPage } from './login.page';
import { ModalResetComponent } from './modal-reset/modal-reset.component';
import { ModalRegisterComponent } from './modal-register/modal-register.component';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    LoginPage,
    ModalResetComponent,
    ModalRegisterComponent
  ], entryComponents: [
    ModalRegisterComponent,
    ModalResetComponent
  ]
})
export class LoginPageModule {}
